import { Router } from "express";
import md5 from "md5";

const router = Router();

router.get("/login", (req, res) => {
  if (req.session.user) res.redirect("/");
  else res.render("login");
});

router.post("/login", (req, res, next) => {
  const { email, password } = req.body;
  const query = `select user_id, username, email, salt, password from ilance_users where email="${email}";`;
  const db = global.db;
  db.query(query, (err, data) => {
    if (err) {
      console.log("error while login", err);
      res.status(500).json({ message: "Internal Server Error" });
    } else {
      const user = data[0];
      if (user) {
        const encryptedPassword = md5(md5(password) + user.salt);
        if (encryptedPassword === user.password) {
          req.session.user = { id: user.user_id, name: user.email };
          res.redirect("/");
        } else {
          res.render("login", { err: "invalid username / password" });
        }
      } else {
        res.render("login", { err: "invalid username / password" });
      }
    }
  });
});

export default router;
