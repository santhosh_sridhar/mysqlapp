import { Router } from "express";

const router = Router();

router.get("/", (req, res) => {
  if (req.session.user) {
    const query =
      "select project.project_title,project.date_added, user.username from ilance_projects as project inner join ilance_users as user on project.user_id=user.user_id;";
    const db = global.db;
    db.query(query, (err, data) => {
      if (err) {
        res.status(500).json({ message: "Internal Server Error" });
      } else {
        res.render("home", {
          data,
        });
      }
    });
  } else {
    res.redirect("/login");
  }
});

export default router;
