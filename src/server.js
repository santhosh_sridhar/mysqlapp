import express from "express";
import path from "path";
import mysql from "mysql";
import dotenv from "dotenv";
import session from "express-session";
import indexRouter from "./routes/index";
import userRouter from "./routes/user";

dotenv.config();
const app = express();
const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

db.connect((err) => {
  if (err) throw err;
  else console.log("db connected successfully");
});

global.db = db;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  session({
    secret: process.env.SESSION_SECRET || "secret",
    saveUninitialized: false,
    resave: true,
  })
);
app.use(express.static(path.join(__dirname, "public")));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use("/", indexRouter, userRouter);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`server running at ${port}`);
});
